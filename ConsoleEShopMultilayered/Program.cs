﻿using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered
{
    class Program
    {
        static void Main(string[] args)
        {
            EShopController eShopController = new EShopController();
            eShopController.Run();

        }
    }
}