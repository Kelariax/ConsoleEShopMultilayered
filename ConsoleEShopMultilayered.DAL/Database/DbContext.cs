﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    public class DbContext
    {
        public  List<Account> Accounts { get; set; } = InitialData.Accounts;
        public  List<Good> Goods { get; set; } = InitialData.Goods;
        public  List<Order> Orders { get; set; } = InitialData.Orders;
    }
}
