﻿using ConsoleEShopMultilayered.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    public static class InitialData
    {
        public static List<Account> Accounts { get; set; } = new List<Account>()
        {
            new Account("User", "Password"),
            new Account("admin", "admin", PermisionStatus.Admin)
        };

        public static List<Good> Goods { get; set; } = new List<Good>()
        {
            new Good("T-Shirt", "Clothing", "T-Shirt", Convert.ToDecimal(15.99)),
            new Good("Baseball Cap", "Clothing", "Cap", Convert.ToDecimal(9.99))
        };

        public static List<Order> Orders { get; set; } = new List<Order>()
        {

        };
    }
}
