﻿using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
   public class AccountsRepository : IRepository<Account>, IEnumerable<Account>
    {
        private readonly DbContext db;

        public AccountsRepository(DbContext db)
        {
            this.db = db;
        }
        public Account this[int index]
        {
            get
            {
                if (index >= 0 && index < db.Accounts.Count)
                {
                    return db.Accounts[index];
                }
                throw new ArgumentOutOfRangeException(new string("Index is out of range"));
            }
        }
        public IEnumerable<Account> GetAll()
        {
            return db.Accounts;
        }
        public void Add(Account item)
        {
            if(item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            if (db.Accounts.Contains(item))
            {
                throw new ArgumentException(nameof(item), new string("Account with same login already exists in DB"));
            }
            db.Accounts.Add(item);
        }
        public Account Find(Predicate<Account> predicate)
        {
            if(predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Accounts.Find(predicate);
        }
        public int IndexOf(Predicate<Account> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Accounts.FindIndex(predicate);
        }
        public bool Contains(Account item)
        {
            return db.Accounts.Contains(item);
        }
        public bool Contains(Predicate<Account> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Accounts.Exists(predicate);
        }
        public IEnumerator<Account> GetEnumerator()
        {
            return db.Accounts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return db.Accounts.GetEnumerator();
        }
    }
}
