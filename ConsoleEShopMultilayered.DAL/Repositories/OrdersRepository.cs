﻿using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    public class OrdersRepository : IRepository<Order>
    {
        private readonly DbContext db;
        public OrdersRepository(DbContext db)
        {
            this.db = db;
        }
        public OrdersRepository(IEnumerable<Order> orders)
        {
            db.Orders = new List<Order>(orders);
        }

        public Order this[int index]
        {
            get
            {
                if (index >= 0 && index < db.Orders.Count)
                {
                    return db.Orders[index];
                }
                throw new ArgumentOutOfRangeException(new string("Index is out of range"));
            }
        }
        public IEnumerable<Order> GetAll()
        {
            return db.Orders;
        }
        public Order Find(Predicate<Order> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Orders.Find(predicate);
        }

        public int IndexOf(Predicate<Order> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Orders.FindIndex(predicate);
        }
        public void Add(Order item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(new string("Order is null"));
            }
            if (this.Contains(i => i.Id == item.Id))
            {
                throw new ArgumentException(new string("Order with such id already exists"));
            }
            db.Orders.Add(item);
        }
        public bool Contains(Order item)
        {
            return db.Orders.Contains(item);
        }
        public bool Contains(Predicate<Order> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Orders.Exists(predicate);
        }
    }
}
