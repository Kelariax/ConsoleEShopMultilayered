﻿using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories;

namespace ConsoleEShopMultilayered.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext db = new DbContext();


        private AccountsRepository _accountsRepository;
        private GoodsRepository _goodsRepository;
        private OrdersRepository _ordersRepository;

        private UnitOfWork()
        {
        }

        private static UnitOfWork instance;
        public static UnitOfWork GetInstance()
        {
            if (instance == null)
            {
                instance = new UnitOfWork();
            }
            return instance;
        }

        public AccountsRepository Accounts
        {
            get
            {
                if (_accountsRepository == null)
                {
                    _accountsRepository = new AccountsRepository(db);
                }
                return _accountsRepository;
            }
        }
        public GoodsRepository Goods
        {
            get
            {
                if (_goodsRepository == null)
                {
                    _goodsRepository = new GoodsRepository(db);
                }
                return _goodsRepository;
            }
        }

        public OrdersRepository Orders
        {
            get
            {
                if (_ordersRepository == null)
                {
                    _ordersRepository = new OrdersRepository(db);
                }
                return _ordersRepository;
            }
        }
    }

}
