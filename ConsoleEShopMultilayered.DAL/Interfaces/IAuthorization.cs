﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{ 
    public interface IAuthorization
    {
        public string Login { get; }
        public string Password { get; set; }

    }
}
