﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    public interface IRepository<T> where T : IEntity
    {
        IEnumerable<T> GetAll();
        void Add(T item);

        T Find(Predicate<T> predicate);

        int IndexOf(Predicate<T> predicate);

        bool Contains(T item);

        bool Contains(Predicate<T> predicate);
    }
}
