﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    public interface IEntity
    {
         int Id { get; }
    }
}
