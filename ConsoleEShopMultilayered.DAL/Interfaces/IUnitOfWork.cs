﻿using ConsoleEShopMultilayered.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        public AccountsRepository Accounts { get;}
        public GoodsRepository Goods { get;}
        public OrdersRepository Orders { get;}

    }
}
