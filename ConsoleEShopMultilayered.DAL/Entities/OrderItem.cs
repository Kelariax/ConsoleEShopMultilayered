﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class OrderItem
    {
        public Good Good { get; set; }
        public int GoodQuantity { get; set; }

        public OrderItem(Good good, int goodQuantity)
        {
            Good = good;
            GoodQuantity = goodQuantity;
        }

    }
}
