﻿using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class Good : IEntity
    {
        public static int Count { get; private set; }
        public int Id { get; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        private decimal _price;
        public decimal Price
        {
            get
            {
                return Math.Round(_price, 2);
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentNullException(new string("Price cant be less than 0"));

                }
                _price = value;
            }
        }
        static Good()
        {
            Count = 0;
        }
        public Good()
        {
            Count++;
            Id = Count;
            Name = "Empty";
            Category = "Empty";
            Description = "Empty";
        }
        public Good(string name, string category, string description, decimal price)
            :this()
        {
            Name = name;
            Category = category;
            Price = price;
            Description = description;
        }      
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else if (obj.GetType() != this.GetType())
            {
                return false;
            }
            Good good = (Good)obj;
            if (this.Name == good.Name)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}
