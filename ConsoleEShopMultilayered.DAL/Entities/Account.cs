﻿using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public enum PermisionStatus { RegisteredUser, Admin }
    public class Account : IAuthorization, IEntity
    {
        public PermisionStatus Permision { get; private set; }
        public int Id { get; }
        public static int Count {get; private set;}

        public PersonInfo personInfo { get; set; }
        public List<int> OrdersId { get; private set; }

        public string Login { get; }
        public string Password { get; set; }
        
        static Account()
        {
            Count = 0;
        }
        public Account(string login, string password)
        {
            
            Count++;
            Id = Count;
            Login = login;            
            Password = password;
            Permision = PermisionStatus.RegisteredUser;
            OrdersId = new List<int>();
            personInfo = new PersonInfo();
        }
        public Account(string login, string password, PermisionStatus permision)
            :this(login, password)
        {
            Permision = permision;
        }
        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            else if (obj.GetType() != this.GetType())
            {
                return false;
            }
            Account account = (Account)obj;
            if (this.Login == account.Login)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Login.GetHashCode();
        }

        public override string ToString()
        {
            return new string(Login);
        }
    }
}
