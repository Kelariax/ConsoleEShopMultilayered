﻿using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public enum OrderStatus
    {
        New, CanceledByAdmin, CanceledByUser, Sent, Received, ReceivedPayment, Completed,        
    }
    public class Order : IEntity
    {
        public int UserId { get; }
        public int Id { get; }

        public DateTime OrderTime{ get; }
        public static int Count { get;  private set; }
        public OrderStatus Status { get; set; }

        public bool IsConfirmed { get; set; }
        public List<OrderItem> OrderItems { get; set; }

        static Order()
        {
            Count = 0;
        }
        public Order(int userId)
        {
            OrderItems = new List<OrderItem>();
            Count++;
            Id = Count;
            this.UserId = userId;

            IsConfirmed = false;
            Status = OrderStatus.New;
            OrderTime = DateTime.Now;
        }
        public Order(int userId, OrderItem orderItem)
            :this(userId)
        { 
            OrderItems.Add(orderItem);
        }
    }
}
