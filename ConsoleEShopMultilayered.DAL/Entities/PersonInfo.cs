﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.DAL.Entities
{
    public class PersonInfo
    {
        private string _name = "Empty field";
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if(string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Value is a null, empty or have a white space!");
                }
                _name = value;
            }
        }
        private string _surname = "Empty field";
        public string Surname
        {
            get
            {
                return _surname;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Value is a null, empty or have a white space!");
                }
                _surname = value;
            }
        }
        private string _phone = "Empty field";
        public string Phone 
        {
            get
            {
                return _phone;
            }
            set
            {
                if(string.IsNullOrWhiteSpace(value))
                {
                    _phone = value;
                }
            }   
        }
        private string _email = "Empty field";
        public string Email
        {
            get 
            { 
                return _email; 
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _email = value;
                }
            }
        }

        public PersonInfo()
        {

        }
       
    }
}
