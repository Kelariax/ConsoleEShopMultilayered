﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    public class OrderItemModel
    {
        public GoodModel Good { get; set; }
        public int GoodQuantity { get; set; }

        public OrderItemModel(GoodModel good, int goodQuantity)
        {
            Good = good;
            GoodQuantity = goodQuantity;
        }

        public override string ToString()
        {
            return new string($"Name: {Good.Name} || Price: {Good.Price} || Quantity: {GoodQuantity}");
        }
    }
}
