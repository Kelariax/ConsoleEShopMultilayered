﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    public class GoodModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public override string ToString()
        {
            return new string($"Name: {Name} Id: {Id}\nCategory: {Category}\nDescription: {Description}\nPrice: {Price}$");
        }
    }
}
