﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    public enum PermisionStatusModel { RegisteredUser, Admin }
    public class AccountModel
    {
        public PermisionStatusModel Permision { get; set; }
        public int Id { get; set; }
        public static int Count { get; set; }

        public PersonInfoModel personInfo { get; set; }
        public List<int> OrdersId { get; set; }

        public string Login { get; }
        public string Password { get; set; }
    }
}
