﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    public enum OrderStatusModel
    {
        New, CanceledByAdmin, CanceledByUser, Sent, Received, ReceivedPayment, Completed,
    }
    public class OrderModel
    {
        public int UserId { get; set; }
        public int Id { get; set; }

        public DateTime OrderTime { get; }
        public OrderStatusModel Status { get; set; }

        public bool IsConfirmed { get; set; }
        public List<OrderItemModel> OrderItems { get; set; }
        
        public OrderModel(int UserId)
        {
            OrderItems = new List<OrderItemModel>();
            this.UserId = UserId;

            IsConfirmed = false;
            Status = OrderStatusModel.New;
            OrderTime = DateTime.Now;
        }
        public void Add(OrderItemModel orderItem)
        {
            if (orderItem == null || orderItem.Good == null)
            {
                throw new ArgumentNullException(nameof(orderItem));
            }
            foreach (var item in OrderItems)
            {
                if (item.Good.Equals(orderItem.Good))
                {
                    item.GoodQuantity += orderItem.GoodQuantity;
                    return;
                }
            }
            OrderItems.Add(orderItem);
        }
        public override string ToString()
        {
            string itemsStr = "";
            decimal sum = 0;
            foreach (var item in OrderItems)
            {
                itemsStr += item.ToString() + "\n";
                sum += item.Good.Price * item.GoodQuantity;
            }
            return new string($"Order Id: {Id}\nDate: {OrderTime.ToShortDateString()} || Order status: {Status} || Is confirmed: {IsConfirmed}\nList of goods:\n{itemsStr}Total price: {Math.Round(sum, 2)}");
        }
    }
}
