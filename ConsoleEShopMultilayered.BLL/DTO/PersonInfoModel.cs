﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.DTO
{
    public class PersonInfoModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return new string($"Name: {Name}\nSurname: {Surname}\nPhone: {Phone}\nEmail: {Email}");
        }
    }
}
