﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
    public interface IOrdersService
    {
        IUnitOfWork db { get; set; }
        IEnumerable<OrderModel> GetAllOrders();
        IEnumerable<OrderModel> GetAllAccountOrders(int accountId);
        OrderModel GetOrderById(int id);
        void AddGoodToOrder(OrderModel orderModel, OrderItemModel orderItemModel);
        void ConfirmOrder(int id, List<int> ordersId);
        void CancelOrderByUser(int id, List<int> ordersId);
        void CancelOrderByAdmin(int id);
        List<int> CreateOrder(OrderModel orderModel, int accountId);
        void ChangeOrderStatus(int id, OrderStatus newOrderStatus);
        void SetOrderStatusRecieved(int id, List<int> ordersId);
        OrderStatus SelectOrderStatus(int selectedStatus);

    }
}
