﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
    public interface IAccountService
    {
        IUnitOfWork db { get; set;}
        IEnumerable<AccountModel> GetAllAccounts();
        AccountModel EditPersonalInfo(int accountId, string value, int selectedField);
        AccountModel GetAccountByLogin(string login);

        AccountModel RegisterAccount(string login, string password);

        AccountModel LogInVerification(string login, string password);


        
    }
}
