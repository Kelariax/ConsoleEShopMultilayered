﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Interfaces
{
    public interface IGoodsService
    {
        IUnitOfWork db { get; set; }
        IEnumerable<GoodModel> GetAllGoods();
        GoodModel GetGoodByName(string name);
        void AddNewGood(GoodModel goodModel);
        public GoodModel EditGoodInfo(int goodId, string value, int selectedField);
    }
}
