﻿using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Services
{
    public class GoodsService : IGoodsService
    {
        public IUnitOfWork db { get; set; }

        private readonly IMapper mapper;
        public GoodsService()
        {
            db = UnitOfWork.GetInstance();
            mapper = new Mapper(GetMapperConfiguration());
           
        }

        public MapperConfiguration GetMapperConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Good, GoodModel>();
                cfg.CreateMap<GoodModel, Good>();
            });
        }
        public IEnumerable<GoodModel> GetAllGoods()
        {
            return mapper.Map<IEnumerable<Good>, List<GoodModel>>(db.Goods.GetAll());            
        }       
        public void AddNewGood(GoodModel goodModel)
        {
            if (db.Goods.Contains(i => i.Name == goodModel.Name))
            {
                throw new ArgumentException($"Good with name '{goodModel.Name}' already exists!");
            }
            if (goodModel.Name == "Empty" || goodModel.Category == "Empty" || goodModel.Price == 0)
            {
                throw new ArgumentException("Fields name, category, price can't be empty!");
            }
            var good = mapper.Map<Good>(goodModel);
            db.Goods.Add(good);
        }
        public GoodModel GetGoodByName(string name)
        {
            Good good = db.Goods.Find(i => i.Name == name);
            if(good == null)
            {
                throw new ArgumentException($"There is no good with name '{name}'!");
            }
            return mapper.Map<GoodModel>(good);           
        }
        public GoodModel EditGoodInfo(int goodId, string value, int selectedField)
        {
            Good good = db.Goods.Find(i => i.Id == goodId);
            switch (selectedField)
            {
                case 1:
                    good.Name = value;
                    break;
                case 2:
                    good.Category = value;
                    break;
                case 3:
                    good.Description = value;
                    break;
                case 4:
                    good.Price = decimal.Parse(value);
                    break;
                default:
                    throw new ArgumentException("There is no such field!");
            }
            return mapper.Map<GoodModel>(good);
        }
    }
}
