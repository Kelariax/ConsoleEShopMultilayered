﻿using AutoMapper;
using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.DAL.Entities;
using ConsoleEShopMultilayered.DAL.Interfaces;
using ConsoleEShopMultilayered.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.BLL.Services
{
    public class AccountService : IAccountService
    {
        public IUnitOfWork db { get; set; }

        private readonly IMapper mapper;
        public AccountService()
        {
            db = UnitOfWork.GetInstance();
            mapper = new Mapper(GetMapperConfiguration());

        }

        public MapperConfiguration GetMapperConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Account, AccountModel>();
                cfg.CreateMap<AccountModel, Account>();
                cfg.CreateMap<PersonInfo, PersonInfoModel>();
                cfg.CreateMap<PersonInfoModel, PersonInfo>();
            });
        }
        public IEnumerable<AccountModel> GetAllAccounts()
        {

            return mapper.Map<IEnumerable<Account>, List<AccountModel>>(db.Accounts.GetAll());
            
        }
        
        public AccountModel GetAccountByLogin(string login)
        {
            Account account = db.Accounts.Find(i => i.Login == login);
            if (account == null)
            {
                throw new ArgumentException($"There is no good with name '{login}'!");
            }
            return mapper.Map<AccountModel>(account);
        }
        public AccountModel EditPersonalInfo(int accountId, string value, int selectedField)
        {
            Account account = db.Accounts.Find(i => i.Id == accountId);
            switch (selectedField)
            {
                case 1:
                    account.personInfo.Name = value;
                    break;
                case 2:
                    account.personInfo.Surname = value;
                    break;
                case 3:
                    account.personInfo.Phone = value;
                    break;
                case 4:
                    account.personInfo.Email = value;
                    break;
                default:
                    throw new ArgumentException("There is no such field!");
            }
            return mapper.Map<AccountModel>(account);
        }
        public AccountModel RegisterAccount(string login, string password)
        {
            if(db.Accounts.Contains(i => i.Login == login))
            {
                throw new ArgumentException($"Account with login '{login}' already exists!");
            }
            if(password.Length < 8)
            {
                throw new ArgumentException("Password length should be more or equal 8!");
            }
            Account account = new Account(login, password);
            db.Accounts.Add(account);
            return mapper.Map<AccountModel>(account);
        }
        public AccountModel LogInVerification(string login, string password)
        {
            Account account = db.Accounts.Find(i => i.Login == login);
            if(account == null || account.Password != password)
            {
                throw new ArgumentException("Invalid login or password!");
            }
            return mapper.Map<AccountModel>(account);
        }
    }
}
