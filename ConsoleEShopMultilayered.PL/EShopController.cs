﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.PL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered
{

    public class EShopController
    {
        public AccountModel account;
        bool exit = false;

        public EShopController()
        {
        }
        public void Run()
        {
            account = null;
            UserActions userMenu = null;
            
            int command;
            while (!exit)
            {
                command = 0;
                try
                {
                    if (account == null)
                    {
                        userMenu = new GuestActions(account);
                        userMenu.ExitNotify += ExitHandler;
                        ((GuestActions)userMenu).LogInNotify += LogInHandler;
                        ((GuestActions)userMenu).RegisterUserNotify += RegisterUserHandler;
                        userMenu.OutputUserFunctions();

                        Console.Write("Choose command: ");

                        command = Convert.ToInt32(Console.ReadLine());

                        Console.Clear();

                        userMenu.ExecuteCommand(command);
                    }
                    else
                    {
                        
                        switch (account.Permision)
                        {
                            case PermisionStatusModel.RegisteredUser:

                                userMenu = new ClientActions(account);
                                ((RegisteredActions)userMenu).LogOutNotify += LogOutHandler;
                                userMenu.ExitNotify += ExitHandler;
                                ((ClientActions)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();

                                userMenu.ExecuteCommand(command);

                                break;
                            case PermisionStatusModel.Admin:
                                userMenu = new AdminActions(account);
                                ((RegisteredActions)userMenu).LogOutNotify += LogOutHandler;
                                userMenu.ExitNotify += ExitHandler;
                                ((AdminActions)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();

                                userMenu.ExecuteCommand(command);
                                break;
                        }
                    }


                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!userMenu.ContinueCheck())
                    {
                        break;
                    }
                }
                Console.Clear();
            }
        }

        public void LogInHandler(object sender, EventArgs e)
        {
            account = sender as AccountModel;
        }

        public void RegisterUserHandler(object sender, EventArgs e)
        {
            account = sender as AccountModel;
        }

        public void LogOutHandler(object sender, EventArgs e)
        {
            account = sender as AccountModel;
        } 
        public void ExitHandler(object sender, EventArgs e)
        {
            exit = Convert.ToBoolean(sender);
        }
    }
}
