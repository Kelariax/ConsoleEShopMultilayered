﻿using ConsoleEShopMultilayered.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShopMultilayered.PL
{
    abstract class RegisteredActions : UserActions
    {

        public EventHandler LogOutNotify { get; set; }
        protected RegisteredActions(AccountModel account)
            : base(account)
        {
            Functions.Add(3, this.CreateOrder);
            Functions.Add(4, this.ConfirmOrder);
            Functions.Add(9, this.LogOut);
        }

        public void CreateOrder()
        {
            OrderModel order = new OrderModel(Account.Id);
            GoodModel goodModel = null;
            do
            {
                Console.WriteLine("<<Create order>>");
                Console.WriteLine(order);
                Console.Write("\nEnter good name: ");
                string name = Console.ReadLine();
                try
                {
                    goodModel = GoodsService.GetGoodByName(name);
                    int quantity;
                    Console.Write("Enter quantity of good: ");
                    quantity = int.Parse(Console.ReadLine());

                    OrdersService.AddGoodToOrder(order, new OrderItemModel(goodModel, quantity));

                    Console.Clear();
                    Console.WriteLine("<<Create order>>");
                    Console.WriteLine(order);
                    Console.WriteLine("\nGood was added to the order");
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    if (order.OrderItems.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("<<Create order>>");
                        Console.WriteLine(order);

                        Console.WriteLine("\nOrder successfully created");
                        Console.WriteLine("\nPress any key...");
                        Account.OrdersId = OrdersService.CreateOrder(order, Account.Id);
                        Console.ReadKey();
                    }
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);
        }

        public void ConfirmOrder()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order confirming>>");
                Console.Write("Enter order id: ");
                try
                {
                    id = int.Parse(Console.ReadLine());
                    OrdersService.ConfirmOrder(id, Account.OrdersId);
                    Console.WriteLine("Order successfully confirmed!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);                    
                }
                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);
        }

        protected void EditPersonalInfo(AccountModel account)
        {
            int command;
            do
            {
                Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                Console.WriteLine(account.personInfo);
                Console.WriteLine("\n1 - Name\n2 - Surname\n3 - Phone\n4 - Email\n5 - Back");
                Console.Write("Choose command: ");
                try
                {
                    command = int.Parse(Console.ReadLine());
                    if(command == 5)
                    {
                        break;
                    }
                    Console.WriteLine("Enter value: ");
                    account = AccountService.EditPersonalInfo(account.Id, Console.ReadLine(), command);
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);

                    Console.WriteLine("Press any key...");
                    Console.ReadKey();
                }
                Console.Clear();
            } while (true);
        }

        public void LogOut()
        {
            LogOutNotify?.Invoke(null, new EventArgs());
        }
    }
}
