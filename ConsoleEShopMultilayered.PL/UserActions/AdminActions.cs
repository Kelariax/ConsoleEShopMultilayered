﻿using ConsoleEShopMultilayered.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL
{
    class AdminActions : RegisteredActions
    {

        public AdminActions(AccountModel account)
            : base(account)
        {
            Functions.Add(5, this.ChangeUserInfo);
            Functions.Add(6, this.CreateNewGood);
            Functions.Add(7, this.ChangeGoodInfo);
            Functions.Add(8, this.ChangeOrderStatus);
        }
        public override void OutputUserFunctions() => Console.WriteLine("<<Console E-Shop>><<User>>\n1 - Show all goods\n2 - Search good by name\n3 - Make new order\n" +
            "4 - Confirm order\n5 - Change user's info\n6 - Add new good\n7 - Change good info\n8 - Change order status\n9 - Log out\n0 - Exit");
        
        public void ChangeUserInfo()
        {
            AccountModel account = null;
            do
            {
                if (account != null)
                {
                    EditPersonalInfo(account);
                    account = null;
                    Console.WriteLine("\nAny key - Continue with another user\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    try
                    {
                        Console.WriteLine("<<Editing user's info>>");
                        Console.WriteLine("\nEnter user's login: ");
                        account = AccountService.GetAccountByLogin(Console.ReadLine());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine();
                        Console.WriteLine(ex.Message);

                        Console.WriteLine("Press any key...");
                        Console.ReadKey();
                    }
                }
                Console.Clear();
            } while (true);
        } 

        public void CreateNewGood()
        {
            GoodModel good = new GoodModel();
            int command;
            do
            {
                Console.WriteLine("<<Good creation>>");
                Console.WriteLine(good);
                Console.WriteLine("\n1 - Edit good's field\n2 - Add good\n3 - Back");
                Console.Write("Choose command: ");
                try
                {
                    command = int.Parse(Console.ReadLine());

                    switch (command)
                    {
                        case 1:
                            Console.Clear();
                            EditGood(good);
                            break;
                        case 2:

                            GoodsService.AddNewGood(good);
                            Console.Clear();

                            Console.WriteLine("<<Good creation>>");
                            Console.WriteLine(good);
                            Console.WriteLine($"\nGoods successfully added!");
                            Console.WriteLine("\nPress any key...");
                            Console.ReadKey();
                            return;
                        case 3:
                            return;
                        default:
                            Console.WriteLine("There is no such command!");
                            Console.ReadKey();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);

                    Console.WriteLine("Press any key...");
                    Console.ReadKey();
                }
                Console.Clear();
            } while (true);
        }
        
        public void ChangeGoodInfo()
        {
            GoodModel goodModel = null;
            do
            {
                if (goodModel != null)
                {
                    Console.Clear();
                    EditGood(goodModel);
                    goodModel = null;
                    Console.WriteLine("\nAny key - Continue with another good\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("<<Editing good's info>>");
                    Console.WriteLine("\nEnter good's name: ");
                    try
                    {
                        goodModel = GoodsService.GetGoodByName(Console.ReadLine());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine();
                        Console.WriteLine(ex.Message);

                        Console.WriteLine("Press any key...");
                        Console.ReadKey();
                    }                   
                }
                Console.Clear();
            } while (true);
        }        
        private void EditGood(GoodModel goodModel)
        {
            int command;            
            do
            {
                Console.WriteLine("<<Good editing>>");
                Console.WriteLine(goodModel);
                Console.WriteLine("\n1 - Name\n2 - Category\n3 - Description\n4 - Price\n5 - Back");
                Console.Write("Choose command: ");
                try
                {
                    command = int.Parse(Console.ReadLine());
                    if (command == 5)
                    {
                        break;
                    }
                    Console.WriteLine("Enter value: ");
                    goodModel = GoodsService.EditGoodInfo(goodModel.Id, Console.ReadLine(), command);
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);

                    Console.WriteLine("Press any key...");
                    Console.ReadKey();
                }                
                Console.Clear();
            } while (true);
        }
        public void ChangeOrderStatus()
        {
            OrderModel order = null;
            do
            {
                try
                {
                    if (order != null)
                    {
                        Console.WriteLine("<<Changing order status>>");
                        Console.WriteLine(order);

                        Console.WriteLine("\n1 - Canceled by admin\n2 - Sent\n3 - Received payment\n4 - Completed");
                        Console.Write("Choose status: ");

                        OrdersService.ChangeOrderStatus(order.Id, OrdersService.SelectOrderStatus(int.Parse(Console.ReadLine())));

                        Console.WriteLine("\nAny key - Continue with another good\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("<<Changing order status>>");
                        Console.WriteLine("\nEnter order's id: ");

                        order = OrdersService.GetOrderById(int.Parse(Console.ReadLine()));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);

                    Console.WriteLine("\nAny key - Continue with another good\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                Console.Clear();
            } while (true);
        }    
    }
}
