﻿using ConsoleEShopMultilayered.BLL.DTO;
using ConsoleEShopMultilayered.BLL.Interfaces;
using ConsoleEShopMultilayered.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL
{
    public abstract class UserActions
    {
        public EventHandler ExitNotify { get; set; }
        public AccountModel Account { get; set; }
        public Dictionary<int, Action> Functions { get; set; }
        
        protected IAccountService AccountService { get; }
        protected IGoodsService GoodsService { get; }
        protected IOrdersService OrdersService { get; }



        protected UserActions ()
        {
            Functions = new Dictionary<int, Action> {
                {1, this.OutputGoods },
                {2, this.FindGood },
                {0, this.Exit }
            };
            AccountService = new AccountService();
            GoodsService = new GoodsService();
            OrdersService = new OrdersService();
        }
        protected UserActions(AccountModel account)
            :this()
        {
            this.Account = account;
        }
        public abstract void OutputUserFunctions();
        public void OutputGoods()
        {
            int count = 0;
            int page = 1;
            Console.WriteLine($"<<Goods list>><<Page: {page}>>");
            foreach (var item in GoodsService.GetAllGoods())
            {
                Console.WriteLine(item + "\n");
                count++;
                if (count % 5 == 0)
                {
                    Console.WriteLine("Any key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        break;
                    }
                    Console.Clear();
                    page++;
                    Console.WriteLine($"<<Goods list>><<Page: {page}>>");
                }
            }
            Console.WriteLine("\nPress any key...");
            Console.ReadKey();
        }
        public void FindGood()
        {
            GoodModel goodModel;
            do
            {
                Console.WriteLine("<<Good search>>");
                Console.Write("Enter good name: ");
                try
                {
                    goodModel = GoodsService.GetGoodByName(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine(goodModel);

                   
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);    
        }
        public bool ContinueCheck()
        {
            var symb = Console.ReadKey();
            if (symb.Key == ConsoleKey.Escape)
            {
                return false;
            }
            return true;
        }

        public void ExecuteCommand(int command)
        {
            if(Functions.ContainsKey(command))
            {
                Functions[command].Invoke();
            }
            else
            {
                Console.WriteLine("\nThere is no such command");
                Console.WriteLine("\nPress any key...");
                Console.ReadKey();
            }
        }
        public void Exit()
        {
            ExitNotify?.Invoke(true, new EventArgs());
        }
    }
}
