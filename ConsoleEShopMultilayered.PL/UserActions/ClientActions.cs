﻿using ConsoleEShopMultilayered.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL
{
    class ClientActions : RegisteredActions
    {
        public ClientActions(AccountModel account)
            : base(account)
        {
            Functions.Add(5, CancelOrder);
            Functions.Add(6, ShowOrders);
            Functions.Add(7, SetOrderRecieved);
            Functions.Add(8, ChangePersonalInfo);
        }
        public override void OutputUserFunctions() => Console.WriteLine("<<Console E-Shop>><<User>>\n1 - Show all goods\n2 - Search good by name\n3 - Make new order\n" +
            "4 - Confirm order\n5 - Cancel order\n6 - Show your orders\n7 - Set order status received\n8 - Change personal info\n9 - Log out\n0 - Exit");
        
        public void ShowOrders()
        {
            int count = 0;
            int page = 1;
            Console.WriteLine($"<<{Account.Login} order list>><<Page: {page}>>");
            foreach (var item in OrdersService.GetAllAccountOrders(Account.Id))
            {
                Console.WriteLine(item + "\n");
                count++;
                if (count % 5 == 0)
                {
                    Console.WriteLine("Any key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        break;
                    }
                    Console.Clear();
                    page++;
                    Console.WriteLine($"<<{Account.Login} order list>><<Page: {page}>>");
                }
            }
            Console.ReadLine();
        }
        public void CancelOrder()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order canceling>>");
                Console.Write("Enter order id: ");
                try
                {
                    id = int.Parse(Console.ReadLine());
                    OrdersService.CancelOrderByUser(id, Account.OrdersId);

                    Console.WriteLine($"\nOrder '{id}' successfully canceled!");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("Any key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);
        }

        public void SetOrderRecieved()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order recieving>>");
                Console.Write("Enter order id: ");
                try
                {
                    id = int.Parse(Console.ReadLine());
                    OrdersService.SetOrderStatusRecieved(id, Account.OrdersId);

                    Console.WriteLine("\nOrder successfully received!");
                }
                catch(Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("Any key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);
        }

        public void ChangePersonalInfo()
        {
            EditPersonalInfo(this.Account);
        }
    }
}
