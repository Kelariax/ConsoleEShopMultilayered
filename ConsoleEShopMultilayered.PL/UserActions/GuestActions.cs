﻿using ConsoleEShopMultilayered.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopMultilayered.PL
{
    class GuestActions : UserActions
    {
        public EventHandler LogInNotify { get; set; }
        public EventHandler RegisterUserNotify { get; set; }
        public GuestActions(AccountModel account)
                : base(account)
        {
            Functions.Add(3, this.RegisterUser);
            Functions.Add(4, this.LogIn);            
        }
    
        public override void OutputUserFunctions() => Console.WriteLine("<<Console E-Shop>><<Guest>>\n1 - Show all goods\n2 - Search good by name\n3 - Sign up\n4 - Log in\n0 - Exit");
        public void RegisterUser()
        {
            AccountModel account = null;
            do
            {
                Console.WriteLine("<<User registration>>");
                Console.Write("Enter login: ");
                string login = Console.ReadLine();              
                Console.Write("Enter password: ");
                string password = Console.ReadLine();
                try
                {
                    account = AccountService.RegisterAccount(login, password);

                    Console.WriteLine("\nThanks you for registration!");
                    Console.ReadKey();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                Console.Clear();
            } while (true);
            RegisterUserNotify?.Invoke(account, new EventArgs());
        }
        public void LogIn()
        {
            AccountModel account = null;
            do
            {
                Console.WriteLine("<<User authorization>>");
                Console.Write("Enter login: ");
                string login = Console.ReadLine();
                Console.Write("Enter password: ");
                string password = Console.ReadLine();
                try
                {
                    account = AccountService.LogInVerification(login, password);
                    Console.WriteLine("\nSuccessful authorization!");
                    Console.ReadKey();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                Console.Clear();
            } while (true);
            LogInNotify?.Invoke(account, new EventArgs());
        }
    }
}
